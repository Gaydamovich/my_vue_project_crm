import firebase from 'firebase/app'

export default {
  state: {
    info: {},
  },
  mutations: {
    setInfo(state, info) {
      state.info = info
    },
    clearInfo(state) {
      state.info = {}
    },
  },
  actions: {
    async fetchUserInfo({ dispatch, commit }) {
      try {
        const uid = await dispatch('getUid')
        const info = (
          await firebase.database().ref(`/users/${uid}/info`).once('value')
        ).val()
        commit('setInfo', info)
      } catch (e) {}
    },
    async updateInfo({ dispatch, commit }, updateInfo) {
      try {
        const uid = await dispatch('getUid')
        const newData = { ...this.getters.info, ...updateInfo }
        await firebase.database().ref(`/users/${uid}/info`).update(newData)
        commit('setInfo', newData)
      } catch (e) {
        commit('setError', e)
        throw e
      }
    },
  },
  getters: {
    info(state) {
      return state.info
    },
  },
}
