import Vue from 'vue'
import App from './App.vue'
import Vuelidate from 'vuelidate'
import VueMeta from 'vue-meta'
import router from './router'
import store from './store'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

import dataFilter from '@/filters/data.filter'
import currencyFilter from '@/filters/currency.filter'
import localeFilter from '@/filters/locale.filter'
import tooltip from '@/directives/tooltip.directive'
import alertPlugin from '@/utils/message.plugin'
import titlePlugin from '@/utils/title.plugin'
import Loader from '@/components/Loader'
import Paginate from 'vuejs-paginate'

Vue.config.productionTip = false

Vue.use(Vuelidate)
Vue.use(alertPlugin)
Vue.use(titlePlugin)
Vue.use(VueMeta)
Vue.filter('dateFilter', dataFilter)
Vue.filter('currencyFilter', currencyFilter)
Vue.filter('localeFilter', localeFilter)
Vue.directive('tooltip', tooltip)
Vue.component('Loader', Loader)
Vue.component('Paginate', Paginate)

firebase.initializeApp({
  apiKey: 'AIzaSyCH-J0YZ93LOTZoGp6NCke8wnsoZcOsV3k',
  authDomain: 'vue-app-53ab0.firebaseapp.com',
  projectId: 'vue-app-53ab0',
  storageBucket: 'vue-app-53ab0.appspot.com',
  messagingSenderId: '79997478795',
  appId: '1:79997478795:web:ed64b1fc8b79d926dcb18e',
})

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: (h) => h(App),
    }).$mount('#app')
  }
})
