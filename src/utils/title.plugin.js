import localeFilter from '@/filters/locale.filter'

export default {
  install(Vue) {
    Vue.prototype.$title = function (key) {
      return `${localeFilter(key)} | ${process.env.VUE_APP_TITLE}`
    }
  }
}