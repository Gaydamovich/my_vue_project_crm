export default {
  logout: 'Вы вышли из системы',
  login: 'Войдите в систему',
  'auth/wrong-password': 'Неверный пароль',
  'auth/user-not-found': 'Пользователь не найден',
  'auth/email-already-in-use': 'Пользователь c таким адресом уже существует',
}
