import store from '../store'

export default function dataFilter(value) {
  const locale = store.getters.info.locale || 'ru-RU'
  
  const o = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  }

  return new Intl.DateTimeFormat(locale, o).format(new Date(value))
}
