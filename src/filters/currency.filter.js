export default function currencyFilter(value, currency = 'RUB') {
  return new Intl.NumberFormat('ru', {
    style: 'currency',
    maximumFractionDigits: 3,
    currency,
  }).format(value)
}
