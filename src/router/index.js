import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from 'firebase/app'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: { layout: 'main', auth: true },
    component: () => import('../components/Home.vue'),
  },
  {
    path: '/categories',
    name: 'Categories',
    meta: { layout: 'main', auth: true },
    component: () => import('../components/Categories.vue'),
  },
  {
    path: '/history',
    name: 'history',
    meta: { layout: 'main', auth: true },
    component: () => import('../components/History.vue'),
  },
  {
    path: '/planning',
    name: 'planning',
    meta: { layout: 'main', auth: true },
    component: () => import('../components/Planning.vue'),
  },
  {
    path: '/record',
    name: 'record',
    meta: { layout: 'main', auth: true },
    component: () => import('../components/Record.vue'),
  },
  {
    path: '/account',
    name: 'account',
    meta: { layout: 'main', auth: true },
    component: () => import('../components/Account.vue'),
  },
  {
    path: '/profile',
    name: 'profile',
    meta: { layout: 'main', auth: true },
    component: () => import('../components/Profile.vue'),
  },
  {
    path: '/details/:id',
    name: 'details',
    meta: { layout: 'main', auth: true },
    component: () => import('../components/Details.vue'),
  },
  {
    path: '/login',
    name: 'login',
    meta: { layout: 'form' },
    component: () => import('../components/Login.vue'),
  },
  {
    path: '/register',
    name: 'register',
    meta: { layout: 'form' },
    component: () => import('../components/Register.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser
  const requireAuth = to.matched.some((record) => record.meta.auth)

  if (requireAuth && !currentUser) {
    next('/login?message=login')
  } else {
    next()
  }
})

export default router
